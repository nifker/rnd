Generates a string of pseudo-random picked chars from a hard-coded string.  
  
Setup:  
Add ~/.cargo/ (or configured path) to your PATH variable when installing  
> git clone https://gitlab.com/nifker/rnd  
> cd rnd  
> cargo build --release  
> cargo run  

or  

> cargo install  
    
Usage:  
uses default profile  
> rnd  

select json file and the profile  
> rnd json_file profile

Example Json-File:  
[  
    {  
        \"profile\": \"default\",  
        \"length\": 128,  
        \"charlist\": \"1234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz*+~'#;,:._-°!§$%&/()=?{[]}\",  
        \"strings\": 4  
    }  
]  
