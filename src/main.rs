extern crate rand;
extern crate serde_json;

use rand::Rng;

fn get_profile_from_json(profile_id: &str, json_data: &str) -> Option<(u8, String, u16)>{
    use std::string::String;
    use serde_json::Value;

    //create deserializer
    let v: Value = serde_json::from_str(&json_data).unwrap();
    let mut len: Option<u8> = None;
    let mut list: Option<String> = None;
    let mut count: Option<u16> = None;

    //TODO: need find() iterator
    //WORKAROUND
    if v.is_array(){//get array from JSON file
        let arr = v.as_array().unwrap();
        for i in arr.iter(){//iterate
            if i["profile"] == profile_id{//check for profile
                if i["length"].is_u64(){
                    len = Some(i["length"].as_u64().unwrap() as u8);
                }
                if i["charlist"].is_string(){
                    list = Some(i["charlist"].as_str().unwrap().to_string());
                }
                if i["strings"].is_u64(){
                    count = Some(i["strings"].as_u64().unwrap() as u16);
                }
            }
        }
    }

    if len.is_none() || list.is_none() || count.is_none(){
        return None;
    }

    Some((len.unwrap(), list.unwrap(), count.unwrap()))
}

const DEFAULT_PROFILE: &str = "
[
    {
        \"profile\": \"default\",
        \"length\": 128,
        \"charlist\": \"1234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz*+~'#;,:._-°!§$%&/()=?{[]}\",
        \"strings\": 4
    }
]
";


fn main(){
    use std::io::Read;

    let mut rnd: u8;
    let mut c_list: Vec<char>;
    let mut str_output: String;
    let profile: (u8, String, u16);//len, charlist,

    /*
        first arg: JSON file for different profiles
        second arg: profile to be selected

        otherwise use the default profile
    */
    {
        let args: Vec<_> = std::env::args().collect();

        if args.len() == 3{
            //load JSON file into String
            let mut file_content = String::new();
            std::fs::File::open(args[1].clone()).unwrap().read_to_string(&mut file_content).unwrap();

            //load profile from JSON file
            profile = get_profile_from_json(&args[2].clone(), &file_content).unwrap();
        }
        //use default profile
        else if args.len() == 1{
            profile = get_profile_from_json("default", DEFAULT_PROFILE).unwrap();
        }
        else{
            panic!("Insufficient number of args");
        }
    }

    //get Vec<char> from String
    c_list = Vec::with_capacity(profile.1.len());
    for c in profile.1.chars(){
        c_list.push(c);
    }

    for _c in 0..profile.2{//number of strings

        str_output = String::with_capacity(profile.0 as usize);
        for _i in 0..profile.0 {//length of the string
            rnd = rand::thread_rng().gen_range(0, c_list.len() as u8);
            str_output.push(c_list[rnd as usize]);
        }

        println!("{}", str_output);

        str_output.clear();
    }
}
